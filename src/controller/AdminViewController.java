package controller;

import java.io.IOException;
import java.util.List;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.input.KeyCode;
import javafx.util.Callback;
import main.Photos;
import model.Photo;
import model.User;
import model.UserData;

public class AdminViewController implements Logoutable
{
	@FXML private TableView table;
	@FXML private TableColumn usernameColumn;
	@FXML private TableColumn nameColumn;
	@FXML private TextField inputUsername;
	@FXML private TextField inputName;
	@FXML private Button deleteButton;

	private UserData data;
	private List<User> userList;
	private ObservableList<User> oList;

	@FXML private void initialize() throws IOException, ClassNotFoundException
	{
		Photos.stage.setTitle("admin");
		data = UserData.read();
		userList = data.getUserList();
		oList = FXCollections.observableArrayList(userList);
		usernameColumn.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<User, String>, ObservableValue<String>>() {
			public ObservableValue<String> call(TableColumn.CellDataFeatures<User, String> u) {
				return new SimpleStringProperty(u.getValue().getUsername());
			}
		});
		nameColumn.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<User, String>, ObservableValue<String>>() {
			public ObservableValue<String> call(TableColumn.CellDataFeatures<User, String> u) {
				return new SimpleStringProperty(u.getValue().getName());
			}
		});
		inputName.setOnKeyPressed(event -> {if(event.getCode()== KeyCode.ENTER) handleAdd();});
		inputUsername.setOnKeyPressed(event -> {if(event.getCode()== KeyCode.ENTER) handleAdd();});
		table.setOnKeyPressed(event -> {if(event.getCode()== KeyCode.DELETE) deleteButton.fire();});
		table.setItems(oList);
	}


	@FXML private void handleLogout()
	{
		logout();
	}
	@FXML private void handleAdd()
	{
		Alert alert = new Alert(Alert.AlertType.INFORMATION, "Please enter your Name AND desired Username", ButtonType.OK);
		AlertTrimmer.trim(alert, "Invalid input");
		if(inputUsername.getText().trim().isEmpty() || inputName.getText().trim().isEmpty()){
			alert.show();
		} else if(data.getUser(inputUsername.getText().trim())!=null){
			alert.setContentText("That Username is unavaialable");
			alert.show();
		} else {
			User temp = new User(inputUsername.getText().trim(), inputName.getText().trim());
			userList.add(temp);
			oList.add(temp);
			try{
				UserData.write(data);
			} catch (IOException e){
				e.printStackTrace();
			}
		}
		inputUsername.clear();
		inputName.clear();
	}
	@FXML private void handleDelete(){
		deleteButton.setTooltip(new Tooltip("delete currently selected user"));
		deleteButton.setOnAction(event -> {
			User temp = (User) table.getSelectionModel().getSelectedItem();
			Alert alert = (temp.getUsername().equals("admin")) ? new Alert(Alert.AlertType.INFORMATION) : new Alert(Alert.AlertType.CONFIRMATION);
			AlertTrimmer.trim(alert, "Delete user (no take-backsies)");
			if (alert.getAlertType() == Alert.AlertType.INFORMATION)
			{
				alert.setContentText("Cannot delete admin");
				alert.show();
			}
			else
			{
				alert.setContentText("Delete user: " + temp.getUsername() + "?");
				alert.showAndWait().ifPresent(response -> {
					if (response == ButtonType.OK)
					{
						oList.remove(temp);
						userList.remove(temp);
						try{
							UserData.write(data);
						} catch (IOException e){
							e.printStackTrace();
						}
					}
				});

			}
		});
	}
}
