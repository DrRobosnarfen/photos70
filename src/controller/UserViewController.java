package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.PointLight;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.scene.input.KeyCode;
import javafx.util.Callback;
import main.Photos;
import model.Album;
import model.Photo;
import model.User;
import model.UserData;

public class UserViewController implements Logoutable
{
	@FXML private TextField inputRename;
	@FXML private Button deleteButton;
	@FXML private ListView listGrid;

	private ObservableList<Album> oList;
	private List<Album> albums = new ArrayList<>();
	private UserData data;
	private User user;

	@FXML private void initialize() throws ClassNotFoundException, IOException
	{
		data = UserData.read();
		listGrid.setOnKeyPressed(event -> {if(event.getCode()== KeyCode.DELETE) deleteButton.fire();});
		inputRename.setOnKeyPressed(event -> {if(event.getCode()== KeyCode.ENTER) handleRename();});
		listGrid.setOnMouseMoved(event -> {if(oList.size()>0 && oList.get(oList.size()-1)==null) oList.remove(oList.size()-1);});
		listGrid.setOnMouseClicked(click -> {
			if(click.getClickCount()==2){
				try{
					Album selectedAlbum = (Album) listGrid.getSelectionModel().getSelectedItem();
					FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/AlbumViewer.fxml"));
					Parent root = loader.load();
					AlbumViewerController control = loader.getController();
					control.passData(selectedAlbum, data, user);
					Photos.stage.setScene(new Scene(root));
				} catch(IOException e){
					e.printStackTrace();
				}

			}
		});
	}

	public void passData(User u, UserData data) throws IOException{
		user = u;
		this.data = data;
		Photos.stage.setTitle(user.getUsername());
		albums = user.getAlbums();
		oList = FXCollections.observableArrayList(albums);
		listGrid.setCellFactory(new Callback<ListView, ListCell>()
		{
			@Override
			public ListCell call(ListView param)
			{
				return new AlbumCell();
			}
		});
		listGrid.setItems(oList);
	}


	@FXML private void handleLogout()
	{
		logout();
	}
	@FXML private void handleRename(){
		Alert alert = new Alert(Alert.AlertType.INFORMATION, "Cannot rename nonexistent albums", ButtonType.OK);
		AlertTrimmer.trim(alert, "Invalid input");
		if(oList.size()==0){
			alert.show();
		} else if(listGrid.getSelectionModel().isEmpty()){
			alert.setContentText("Please select album to rename");
			alert.show();
		} else if(inputRename.getText().trim().isEmpty()){
			alert.setContentText("New name must contain characters");
			alert.show();
		} else if(user.getAlbum(inputRename.getText().trim())!=null) {
			alert.setContentText("Album of the same name already exists");
			alert.show();
		} else {
			Album temp = (Album) listGrid.getSelectionModel().getSelectedItem();
			albums.get(albums.indexOf(temp)).rename(inputRename.getText().trim());
			temp.rename(inputRename.getText().trim());
			inputRename.clear();
			oList.add(null);
			System.out.println(oList.size());
			try{
				UserData.write(data);
			} catch (IOException e){
				e.printStackTrace();
			}
		}
		inputRename.clear();
	}
	@FXML private void handleAdd(){
		TextInputDialog dialog = new TextInputDialog();
		AlertTrimmer.trim(dialog, "Album Creation");
		Optional<String> response = dialog.showAndWait();
		if(response.isPresent()){
			Alert alert = new Alert(Alert.AlertType.INFORMATION, "New name must contain characters", ButtonType.OK);
			AlertTrimmer.trim(alert, "Invalid input");
			if(response.get().trim().isEmpty())
			{
				alert.show();
			} else if(user.getAlbum(response.get().trim())!=null) {
				alert.setContentText("Album of the same name already exists");
				alert.show();
			} else
			{
				Album a = new Album(response.get().trim());
				albums.add(a);
				oList.add(a);
				try
				{
					UserData.write(data);
				}
				catch (IOException e)
				{
					e.printStackTrace();
				}
			}
		}
	}
	@FXML private void handleDelete(){
		Album temp = (Album) listGrid.getSelectionModel().getSelectedItem();
		Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
		AlertTrimmer.trim(alert, "Delete album (no take-backsies)");
		alert.setContentText("Delete album: " + temp.getName() + "?");
		alert.showAndWait().ifPresent(response -> {
			if (response == ButtonType.OK) {
				oList.remove(temp);
				albums.remove(temp);
				try{
					UserData.write(data);
				} catch (IOException e){
					e.printStackTrace();
				}
			}
		});
	}
}
