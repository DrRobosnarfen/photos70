package controller;


import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import javafx.scene.Scene;
import java.io.IOException;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import main.Photos;
import model.User;
import model.UserData;

public class LoginScreenController
{
	@FXML private TextField userNameField;
	@FXML private Label errorText;

	@FXML private void initialize()
	{
		userNameField.setOnKeyPressed(event -> {if(event.getCode()== KeyCode.ENTER) handleLogin();});
	}
 	@FXML private void handleLogin()
	{
		String username = userNameField.getText();
		Parent root;
		UserData data = null;
		try{
			data = UserData.read();
		} catch (Exception e){
			e.printStackTrace();
		}
		try{
			if(data.getUser(username)!=null)
			{
				if (username.equals("admin"))
					root = FXMLLoader.load(getClass().getResource("/view/AdminView.fxml"));
				else
				{
					FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/UserView.fxml"));
					root = loader.load();
					UserViewController control = loader.getController();
					control.passData(data.getUser(username), data);
				}
				Photos.stage.setScene(new Scene(root));
			} else
				errorText.setVisible(true);
		} catch (IOException e){
			e.printStackTrace();
		}
	}
}
