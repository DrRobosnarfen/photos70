package controller;


import java.beans.EventHandler;
import java.io.IOException;
import java.util.Optional;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TextInputDialog;
import javafx.scene.image.ImageView;
import main.Photos;
import model.Album;
import model.Photo;
import model.User;
import model.UserData;

public class PhotoViewerController
{

	private Album album;
	private User user;
	private UserData data;
	private Photo myPhoto;
	private int index;
	@FXML private Label indexHelper;
	@FXML private ImageView photoDisplay;
	@FXML private Label photoCaption;

	@FXML private void initialize(){

	}
	public void passData(Photo p, Album a, User u){
		myPhoto = p;
		album = a;
		user = u;
		photoDisplay.setImage(myPhoto.getImage());
		index = album.getPhotos().indexOf(myPhoto);
		indexHelper.setText("index: " + index + " out of " + (album.getPhotos().size()-1));
		photoCaption.setText(myPhoto.getCaption());
	}

	@FXML private void handleBack() throws IOException
	{
		FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/AlbumViewer.fxml"));
		Parent root = loader.load();
		AlbumViewerController control = loader.getController();
		control.passData(album, data, user);
		Photos.stage.setScene(new Scene(root));
	}
	private void updatePhotoState(){
		myPhoto = album.getPhotos().get(index);
		photoDisplay.setImage(myPhoto.getImage());
		indexHelper.setText("index: " + index + " out of " + (album.getPhotos().size()-1));
		photoCaption.setText(myPhoto.getCaption());
	}
	@FXML private void previousPhoto(){
		if(index!=0)
			index--;
		updatePhotoState();
	}
	@FXML private void nextPhoto(){
		if(index!=album.getPhotos().size()-1)
			index++;
		updatePhotoState();
	}
	@FXML private void handleCaption(){
		TextInputDialog dialog = new TextInputDialog();
		dialog.setWidth(100);
		dialog.setHeight(40);
		AlertTrimmer.trim(dialog, "new Caption");
		Optional<String> response = dialog.showAndWait();
		if(response.isPresent()){
			String cap = response.get().trim();
			myPhoto.setCaption(cap);
			try
			{
				UserData.write(data);
			}
			catch (IOException e)
			{
				e.printStackTrace();
			}
		}
		photoCaption.setText(myPhoto.getCaption());
	}
}
