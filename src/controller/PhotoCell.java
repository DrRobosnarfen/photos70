package controller;

import javafx.scene.control.ListCell;
import model.AlbumCellData;
import model.Photo;
import model.PhotoCellData;

public class PhotoCell extends ListCell<Photo>
{
	private final PhotoCellData data = new PhotoCellData();
	@Override
	public void updateItem(Photo photo, boolean empty) {
		super.updateItem(photo, empty);
		if(photo==null || empty)
		{
			setGraphic(null);
			return;
		}
		data.setNodes(photo);
		setGraphic(data.getAnchor());
	}
}