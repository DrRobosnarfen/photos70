package controller;

import javafx.scene.control.Alert;
import javafx.scene.control.Dialog;

public class AlertTrimmer
{
	public static void trim(Dialog a, String title){
		a.setTitle(title);
		a.setHeaderText(null);
		a.setGraphic(null);
	}
}
