package controller;

import java.io.IOException;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import main.Photos;

public interface Logoutable
{
	default void logout() throws ClassCastException
	{
		try
		{
			Photos.stage.setTitle("PhotoApp");
			Photos.stage.setScene(new Scene(FXMLLoader.load(getClass().getResource("/view/LoginScreen.fxml"))));
		}
		catch (IOException e){
			e.printStackTrace();
		}
	}
}
