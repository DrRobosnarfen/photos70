package controller;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.stage.FileChooser;
import javafx.embed.swt.SWTFXUtils;
import javafx.util.Callback;
import javax.imageio.ImageIO;
import main.Photos;
import model.Album;
import model.Photo;
import model.SerializableImage;
import model.User;
import model.UserData;

public class AlbumViewerController
{
	@FXML private ListView listGrid;
	@FXML private DatePicker inputDate1;
	@FXML private DatePicker inputDate2;
	@FXML private TextField inputTagFilter;
	@FXML private Label albumNameLabel;

	private ObservableList<Photo> oList;
	private List<Photo> photos = new ArrayList<>();
	private Album album;
	private User user;
	private UserData data;

	@FXML private void initialize(){
		listGrid.setOnMouseClicked(click -> {
			if(click.getClickCount()==2){
				try{
					Photo selectedPhoto = (Photo) listGrid.getSelectionModel().getSelectedItem();
					FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/PhotoViewer.fxml"));
					Parent root = loader.load();
					PhotoViewerController control = loader.getController();
					control.passData(selectedPhoto, album, user);
					Photos.stage.setScene(new Scene(root));
				} catch(IOException e){
					e.printStackTrace();
				}

			}
		});
	}

	public void passData(Album a, UserData data, User u){
		album =a;
		user = u;
		this.data = data;

		albumNameLabel.setText(album.getName());
		photos = album.getPhotos();


		oList = FXCollections.observableArrayList(photos);
		listGrid.setCellFactory(new Callback<ListView, ListCell>()
		{
			@Override
			public ListCell call(ListView param)
			{
				return new PhotoCell();
			}
		});
		listGrid.setItems(oList);
	}

	@FXML public void handleAdd() throws IOException{
		FileChooser fileChooser = new FileChooser();
		FileChooser.ExtensionFilter filterPNG = new FileChooser.ExtensionFilter("PNG files (*.png)", "*.PNG");
		FileChooser.ExtensionFilter filterJPG = new FileChooser.ExtensionFilter("JPG files (*.jpg)", "*.JPG");
		fileChooser.getExtensionFilters().addAll(filterPNG, filterJPG);
		fileChooser.setTitle("Upload Photo");

		File file = fileChooser.showOpenDialog(Photos.stage);
		if (file == null)
			return;

		BufferedImage bufferedImage = ImageIO.read(file);
		Image image = SerializableImage.writeImage(bufferedImage);
		SerializableImage temp = new SerializableImage();
		temp.readImage(image);

		for (Photo p: album.getPhotos()) {
			if (temp.equals(p.getSerializableImage())) {
				return;
			}
		}
		boolean breaker = false;
		Photo tempPhoto = new Photo(image);
		for (Album a: user.getAlbums()) {
			for (Photo p: a.getPhotos()) {
				if (temp.equals(p.getSerializableImage())) {
					tempPhoto = p;
					breaker = true;
					break;
				}
			}
			if(breaker)
				break;
		}

		album.addPhoto(tempPhoto);
		oList.add(tempPhoto);
		UserData.write(data);
	}

	@FXML public void handleDelete(){
		Photo temp = (Photo) listGrid.getSelectionModel().getSelectedItem();
		Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
		AlertTrimmer.trim(alert, "Delete photo (no take-backsies)");
		alert.setContentText("Are you sure you want to delete this photo?");
		alert.showAndWait().ifPresent(response -> {
			if (response == ButtonType.OK) {
				oList.remove(temp);
				album.removePhoto(temp);
				try{
					UserData.write(data);
				} catch (IOException e){
					e.printStackTrace();
				}
			}
		});
	}
	@FXML public void handleSearch(){

	}
	@FXML public void handleMove(){
		Photo temp = (Photo) listGrid.getSelectionModel().getSelectedItem();
		Dialog<ButtonType> dialog = new Dialog<>();
		AlertTrimmer.trim(dialog, "Move Photo");
		dialog.setHeaderText("Move this photo which album?");

		List<String> albumNames = new ArrayList<String>();
		for(Album a : user.getAlbums())
		{
			if(a.getName()== album.getName())
				continue;
			albumNames.add(a.getName());
		}
		if(albumNames.size()==0){
			dialog.setHeaderText("Make new albums first");
			return;
		}
		ComboBox<String> cbox = new ComboBox<String>(FXCollections.observableArrayList(albumNames));

		ButtonType buttonTypeCopy = new ButtonType("Copy (duplicate)", ButtonBar.ButtonData.OK_DONE);
		ButtonType buttonTypeMove = new ButtonType("Move (delete)", ButtonBar.ButtonData.OK_DONE);
		dialog.getDialogPane().getButtonTypes().add(buttonTypeMove);
		dialog.getDialogPane().getButtonTypes().add(buttonTypeCopy);
		Optional<ButtonType> response = dialog.showAndWait();
		if(response.isPresent()){
			String albumString = cbox.getSelectionModel().getSelectedItem();
			Album albumDest = user.getAlbum(albumString);
			albumDest.addPhoto(temp);
			if(response.equals(buttonTypeMove)){
				oList.remove(temp);
				album.removePhoto(temp);
			}
			try{
				UserData.write(data);
			} catch (IOException e){
				e.printStackTrace();
			}
		}
	}
	@FXML public void handleDuplicateFiltered(){

	}
	@FXML public void handleBack() throws IOException
	{
		FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/UserView.fxml"));
		Parent root = loader.load();
		UserViewController control = loader.getController();
		control.passData(user, data);
		Photos.stage.setScene(new Scene(root));
	}
}
