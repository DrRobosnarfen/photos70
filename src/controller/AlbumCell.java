package controller;

import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Font;
import model.Album;
import model.AlbumCellData;

public class AlbumCell extends ListCell<Album>
{
	private final AlbumCellData data = new AlbumCellData();

	@Override
	public void updateItem(Album album, boolean empty)
	{
		super.updateItem(album, empty);
		if(album==null || empty){
			setGraphic(null);
			return;
		}
		data.setNodes(album);
		setGraphic(data.getAnchor());

	}
}
