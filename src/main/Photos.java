package main;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import model.User;
import model.UserData;

public class Photos extends Application {
	public static Stage stage;
    @Override
    public void start(Stage primaryStage) throws Exception{
    	UserData data = UserData.read();
	    if(data==null || data.getUser("admin")==null){
		    User admin = new User("admin", "The Techlead");
		    data = new UserData();
		    data.AddUser(admin);
		    UserData.write(data);
	    }


        Parent root = FXMLLoader.load(getClass().getResource("/view/loginScreen.fxml"));
        stage = primaryStage;
        primaryStage.setTitle("PhotoApp");
        primaryStage.setScene(new Scene(root));
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }

}
