package model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javafx.scene.image.Image;

public class Album implements Serializable
{

	private static final long serialVersionUID = 10001100100011001L;
	private String name;
	private List<Photo> photos = new ArrayList<>();
	private Photo oldestPhoto;
	private Photo newestPhoto;

	public Album(String name){
		this.name = name;
		oldestPhoto = null;
		newestPhoto = null;
	}
	public String getName(){
		return name;
	}
	public void rename(String name){
		this.name = name;
	}
	public List<Photo> getPhotos()
	{
		return photos;
	}
	public Image getAlbumPhoto(){
		if(photoCount()==0)
			return null;
		return photos.get(0).getImage();
	}

	public void addPhoto(Photo p){
		photos.add(p);
		findOldestPhoto(p);
		findNewestPhoto(p);
	}
	public void removePhoto(Photo p){
		if(oldestPhoto == p){
			photos.remove(p);
			findOldestPhoto();
			return;
		}
		if(newestPhoto == p){
			photos.remove(p);
			findNewestPhoto();
			return;
		}
		photos.remove(p);
	}
	public int photoCount(){
		return photos.size();
	}

	public void findOldestPhoto(){
		if(photos.size()==0){
			oldestPhoto = null;
			return;
		}
		oldestPhoto = photos.get(0);
		for(Photo p : photos){
			oldestPhoto = (oldestPhoto.getCalendar().compareTo(p.getCalendar()) < 0) ? p : oldestPhoto;
		}
	}
	public void findOldestPhoto(Photo p){
		if(oldestPhoto==null){
			oldestPhoto = p;
			return;
		}
		oldestPhoto = (oldestPhoto.getCalendar().compareTo(p.getCalendar()) < 0) ? p : oldestPhoto;
	}
	public void findNewestPhoto(){
		if(photos.size()==0){
			newestPhoto = null;
			return;
		}
		newestPhoto = photos.get(0);
		for(Photo p : photos){
			newestPhoto = (newestPhoto.getCalendar().compareTo(p.getCalendar()) > 0) ? p : newestPhoto;
		}
	}
	public void findNewestPhoto(Photo p){
		if(newestPhoto==null){
			newestPhoto = p;
			return;
		}
		newestPhoto = (newestPhoto.getCalendar().compareTo(p.getCalendar()) > 0) ? p : newestPhoto;
	}
	public String getDateRange(){
		if(oldestPhoto == null || newestPhoto == null)
			return "";
		return oldestPhoto.getDate() + "-" + newestPhoto.getDate();
	}
}
