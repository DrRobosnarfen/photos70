package model;

import java.io.IOException;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;

public class AlbumCellData
{
	@FXML private AnchorPane anchor;
	@FXML private ImageView albumImage;
	@FXML private Label albumNameLabel;
	@FXML private Label dateRangeLabel;
	@FXML private Label photoCountLabel;

	public AlbumCellData(){
		FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/AlbumCell.fxml"));
		loader.setController(this);
		try{
			anchor = loader.load();
		} catch (IOException e){
			e.printStackTrace();
		}
	}
	public void setNodes(Album a){
			albumImage.setImage(a.getAlbumPhoto());
			albumNameLabel.setText("Album name: " + a.getName());
			dateRangeLabel.setText(a.getDateRange());
			photoCountLabel.setText("Number of Photos: " + a.photoCount());
	}
	public AnchorPane getAnchor(){
		return anchor;
	}
}
