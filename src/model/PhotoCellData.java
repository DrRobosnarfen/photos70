package model;

import java.io.IOException;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;

public class PhotoCellData
{
	@FXML private AnchorPane anchor;
	@FXML private ImageView aPhoto;
	@FXML private Label captionLabel;

	public PhotoCellData(){
		FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/PhotoCell.fxml"));
		loader.setController(this);
		try{
			anchor = loader.load();
		} catch (IOException e){
			e.printStackTrace();
		}
	}
	public void setNodes(Photo p){
		aPhoto.setImage(p.getImage());
		captionLabel.setText(p.getCaption());
	}
	public AnchorPane getAnchor(){
		return anchor;
	}
}
