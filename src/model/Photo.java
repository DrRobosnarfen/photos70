package model;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class Photo implements Serializable
{
	private static final long serialVersionUID = 5883754559491218949L;
	private SerializableImage image;
	private String caption;
	private List<Tag> tags = new ArrayList<>();
	private Calendar calendar;

	public Photo(){
		caption = "";
		calendar = Calendar.getInstance();
		calendar.set(Calendar.MILLISECOND, 0);
		image = new SerializableImage();
	}

	public Photo(Image i){
		this();
		image.readImage(i);
	}
	public Image getImage(){
		return image.writeImage();
	}
	public SerializableImage getSerializableImage()
	{
		return image;
	}

	public String getCaption()
	{
		return caption;
	}
	public void setCaption(String s){
		caption = s;
	}
	public Calendar getCalendar()
	{
		return calendar;
	}
	public String getDate(){
		return new SimpleDateFormat("yyyy-MM-dd").format(calendar.getTime());
	}
}
