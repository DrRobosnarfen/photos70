package model;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class UserData implements Serializable
{
	private static final long serialVersionUID = 10001100100011001L;
	private static final String directory = "data";
	private static final String file = "users.data";

	private List<User> users= new ArrayList<>();

	public void AddUser(User u){
		users.add(u);
	}
	public User getUser(String username){
		for(User u: users){
			if(u.getUsername().equals(username))
				return u;
		}
		return null;
	}
	public User getUser(User user){
		for(User u: users){
			if(u == user)
				return u;
		}
		return null;
	}

	public List<User> getUserList()
	{
		return users;
	}
	public static UserData read() throws IOException, ClassNotFoundException{
		ObjectInputStream input = new ObjectInputStream(new FileInputStream(directory + File.separator + file));
		UserData data = (UserData) input.readObject();
		input.close();
		return data;
	}
	public static void write(UserData data) throws IOException{
		ObjectOutputStream output = new ObjectOutputStream(new FileOutputStream(directory + File.separator + file));
		output.writeObject(data);
		output.close();
	}
}
