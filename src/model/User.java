package model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
public class User implements Serializable
{

	private static final long serialVersionUID = -5270338768169450883L;
	private String username, name;
	private List<Album> albums = new ArrayList<>();

	public User(String username, String name){
		this.username = username;
		this.name = name;
	}

	public String getUsername()
	{
		return username;
	}
	public String getName()
	{
		return name;
	}
	public List<Album> getAlbums() {
		return albums;
	}
	public void addAlbum(Album a) {
		albums.add(a);
	}
	public Album getAlbum(String album){
		for(Album a: albums){
			if(a.getName().equals(album))
				return a;
		}
		return null;
	}
	public void removeAlbum(Album a){
		albums.remove(a);
	}
}
