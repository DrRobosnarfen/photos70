package model;

import java.awt.image.BufferedImage;
import java.io.Serializable;
import javafx.scene.image.Image;
import javafx.scene.image.PixelReader;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;

public class SerializableImage implements Serializable
{
	private int height, width;
	private int[][] pixels;

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

	public int[][] getPixels() {
		return pixels;
	}

	public void readImage(Image image) {
		height = ((int) image.getHeight());
		width = ((int) image.getWidth());
		pixels = new int[width][height];

		PixelReader r = image.getPixelReader();
		for (int i = 0; i < width; i++)
			for (int j = 0; j < height; j++)
				pixels[i][j] = r.getArgb(i, j);
	}

	public Image writeImage() {
		WritableImage image = new WritableImage(width, height);

		PixelWriter w = image.getPixelWriter();
		for (int i = 0; i < width; i++)
			for (int j = 0; j < height; j++)
				w.setArgb(i, j, pixels[i][j]);

		return image;
	}
	public static Image writeImage(BufferedImage bi){
		if(bi==null)
			return null;
		WritableImage image = new WritableImage(bi.getWidth(), bi.getHeight());

		PixelWriter w = image.getPixelWriter();
		for (int i = 0; i < bi.getWidth(); i++)
		{
			for (int j = 0; j < bi.getHeight(); j++)
				w.setArgb(i, j, bi.getRGB(i, j));
		}
		return image;
	}

	public boolean equals(SerializableImage si) {
		if (width != si.getWidth() || height != si.getHeight())
			return false;
		for (int i = 0; i < width; i++)
			for (int j = 0; j < height; j++)
				if (pixels[i][j] != si.getPixels()[i][j])
					return false;
		return true;
	}

}